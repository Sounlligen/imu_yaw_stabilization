#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float64.h>
#include <tf/transform_datatypes.h>
#include <math.h>
#include <vector>


ros::Subscriber imu_sub;
ros::Publisher imu_pub;

sensor_msgs::Imu imu_var;

const int array_size = 10000;
//Array containing first imu readings, base for linear estimation
std::vector <double> yaw_array(array_size);
//double yaw_array[array_size];
//Array containing time points, the line will be yaw vs time
std::vector <double> time_array(array_size);
//double time_array[array_size];
//How many samples to we have
int counter;

//Our linearization will be relative to the time of the first message
double time_first_catch;
//The variance of yaw measurement, the 9th element of covariance matrix
double variance;
//Check if the first message was received
bool if_callbacked;
//Check if we have enough samples
bool is_sampling_finished;
//Check if we calculated params
bool are_params_calculated;

//The approximated line will be yaw = a*time + b
double a;
double b;
//Chi^2 (check wikipedia) for estimating how good the linearization was
double chi2;

void imuCallback(const sensor_msgs::ImuConstPtr &imu)
{
    if(!if_callbacked)
    {
        time_first_catch = imu->header.stamp.toSec();
        variance = imu->orientation_covariance[8];
        if_callbacked = true;
        ROS_INFO("Caught first message!");
    }

    if(counter < array_size)
    {
        double roll, pitch;

        time_array.at(counter) = imu->header.stamp.toSec() - time_first_catch;

        //Convert from quaternion to rpy
        tf::Quaternion quat;
        tf::quaternionMsgToTF(imu->orientation, quat);
        tf::Matrix3x3(quat).getRPY(roll, pitch, yaw_array.at(counter));

        counter++;
    }
    else
    {
        is_sampling_finished = true;
    }

    if(are_params_calculated)
    {
        imu_var.header = imu->header;

        imu_var.orientation = imu->orientation;
        imu_var.angular_velocity = imu->angular_velocity;
        imu_var.linear_acceleration = imu->linear_acceleration;

        imu_var.orientation_covariance = imu->orientation_covariance;
        imu_var.angular_velocity_covariance = imu->angular_velocity_covariance;
        imu_var.linear_acceleration_covariance = imu->linear_acceleration_covariance;

        double time;
        double roll, pitch, yaw;

        time = imu->header.stamp.toSec() - time_first_catch;

        //Convert from quaternion to rpy
        tf::Quaternion quat;
        tf::quaternionMsgToTF(imu->orientation, quat);
        tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

        yaw -= a*time + b;
        //The imu publishes -PI to +PI
        if(yaw > M_PI)
        {
            yaw -= M_2_PI;
        }
        if(yaw < -M_PI)
        {
            yaw += M_2_PI;
        }

        //Turn back to quaternion
        quat = tf::createQuaternionFromRPY(roll, pitch, yaw);
        tf::quaternionTFToMsg(quat, imu_var.orientation);

        imu_pub.publish(imu_var);
    }


}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "imu_stabilizer");

    ros::NodeHandle n;
    ros::Rate rate(10);

    imu_sub = n.subscribe<sensor_msgs::Imu>("/imu/data", 1, &imuCallback);
    imu_pub = n.advertise<sensor_msgs::Imu>("/imu/data_stabilized", 1);

    ROS_INFO("Initializing imu yaw stabilizer...");

    if_callbacked = false;
    is_sampling_finished = false;
    are_params_calculated = false;

    counter = 0;
    chi2 = 0;

    while(ros::ok())
    {
        if(!are_params_calculated)
        {
            if(is_sampling_finished)
            {
                ROS_INFO("Sampling finished. Calculating least squares line.");
                double time = ros::Time::now().toSec();

                //Check least square fitting for details
                double S = array_size;
                double Sx = 0;
                double Sy = 0;
                double Sxx = 0;
                double Sxy = 0;

                for(int i = 0; i < array_size; i++)
                {
                    Sx += time_array.at(i);
                    Sy += yaw_array.at(i);
                    Sxx += time_array.at(i)*time_array.at(i);
                    Sxy += time_array.at(i)*yaw_array.at(i);
                }

                double delta = S*Sxx - Sx*Sx;

                a = (S*Sxy - Sx*Sy)/delta;
                b = (Sxx*Sy - Sx*Sxy)/delta;

                for(int i = 0; i < array_size; i++)
                {
                    chi2 += pow(yaw_array.at(i) - a*time_array.at(i) - b, 2);
                }

                chi2 /= variance;

                time -= ros::Time::now().toSec();

                ROS_INFO("Calculation finished. Parameters are: a = %.5f    b = %.5f    chi2 = %.5f", a, b, chi2);
                ROS_INFO("Calculation took %.5f seconds.", time);

                are_params_calculated = true;
            }
        }


        rate.sleep();

        ros::spinOnce();
    }
}
